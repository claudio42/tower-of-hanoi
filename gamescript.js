let disco;

function moverDisco(move) {
  if (!disco) {
    disco = document.getElementById(move);
  } else {
    let torre = document.getElementById(move);
    if (torre.childElementCount === 0) {
      torre.appendChild(disco.lastElementChild);
    } else {
      let movendoDiscoW = disco.lastElementChild.offsetWidth; //objeto movendo
      let posicDiscow = torre.lastElementChild.offsetWidth; //posicionado na torre

      if (movendoDiscoW < posicDiscow)
        //o que está sendo movido sempre vai ser menor que o posicionado, nunca ao contrário
        torre.appendChild(disco.lastElementChild);
      else alert("Opa!, Tente outra Coisa");
    }

    if (torre.childElementCount === 4 && torre.id === "torre_03")
      //todos os 4 itens na torre 3 fecha o game
      alert("Congrats Champs!");

    disco = null;
  }
}

// adicionando handlers de click

let t1 = document.getElementById("torre_01");
t1.addEventListener("click", function () {
  moverDisco("torre_01");
});

let t2 = document.getElementById("torre_02");
t2.addEventListener("click", function () {
  moverDisco("torre_02");
});

var t3 = document.getElementById("torre_03");
t3.addEventListener("click", function () {
  moverDisco("torre_03");
});
